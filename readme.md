python developer

✉️ contacts:

- telegram - [@gmanka3](https://t.me/gmanka3)
- matrix - gmanka:matrix.org
- xmpp - gmankab@conversations.im
- discord - @gmanka
- github - [@gmankab](https://github.com/gmankab)

skills:

⚡ Backend Development:

- FastAPI
- Sanic
- Bottles
- Uvicorn

📜 Databases:

- Tortoise ORM
- SQLAlchemy ORM
- Anything that can be used with these orm (PostgreSQL, SQLite, MySQL, MariaDB, Microsoft SQL Server, Oracle)

🤖 Telegram bots and userbots:

- Pyrogram
- Telethon
- OpenTele
- Aiogram
- Lot of experience with vanilla telegram api keys

📊 Data analysis tools:

- Matplotlib
- Pandas
- Seaborn

💌 Web Parsers:

- aiohttp
- Requests
- Beautiful Soup
- Selenium
- Scrapy

🖼️ GUI Applications toolkits:

- GTK, Libadwaita
- Qt (pyqt, pyside)
- 3d rendering (vtk)

📚 Also:

- Docker
- Github CI / Gitlab CI
- Async coding
- Readable code
- Advanced logging and errors hadling with python rich
- Knowledge of built-in python libraries 
- Shell Scripts for GNU/Linux
- Batch Scripts for Windows
- UI/UX Design for websites, GUI apps, telegram bots
- Coding in neovim (formerly coded in vscode, pycharm)
- Basic lua knowleage, wrote my config for neovim on lua

🌐 Languages:

- Russian: native
- English: B2, fluent in reading, listening, writing, speaking
- Ukrainian: reading, listening, writing, don't speak

💼 Projects:

[anarchy_bot](https://github.com/gmankab/anarchy_bot) - telegram bot that can promote any user to admin

[tg_api_gen](https://github.com/gmankab/tg_api_gen) - library that generates realistic device_model, app_verion, and system_version (android and ios) for telegram userbots

[reposter](https://github.com/gmankab/reposter) - script for forwarding messages from one telegram channel to another in real time, saves the history of post edits in the comments, marks the message as deleted if it is deleted, and also notifies when a stream has started in the channel. There is support for messages with multiple attachments. Simple, intuitive and setup and launching

[yt_stream_recorder](https://github.com/gmankab/yt_stream_recorder) - script for recording YouTube streams. You only need to run it once, and while it is running it will record all streams on the selected channel

[suggest](https://github.com/gmankab/suggest) - bot that allows you to suggest posts to telegram channels. It is possible to ban a user so that he can no longer suggesting posts, and you can choose the time for which the user will be banned. There is support for messages with multiple attachments

[betterdata](https://github.com/gmankab/betterdata) - library for working with data. Features: automatic writing to disk when new data is added to an object, quick export to yml

[PrettyGit](https://github.com/gmankab/PrettyGit) - user-friendly interface for git, supports commits, pushes, and uploading to [pipy.org](https://pypi.org)

[easyselect](https://github.com/gmankab/easyselect) - library that allows the user to select between multiple items in the console using the keyboard. Supports very long lists that don't fit on the screen, [rich styles](https://rich.readthedocs.io/en/stable/style.html), control with buttons `up`, `down`, `left`, `right`, `wasd`, `j`, `h`, `home`, `end`, `page up`, `page down`
